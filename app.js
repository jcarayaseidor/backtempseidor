var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var redis = require('redis');
var app = express();
var http = require('http');
var socketIO = require('socket.io');
var axios = require('axios');
var bluebird = require('bluebird');

// Creando promesas
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);


// view engine setup
//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));





var clientRedis = redis.createClient();

clientRedis.on('ready',()=>{
  console.log("Redis conectado exitosamente");
});
clientRedis.on('error',(err)=>{
  console.log("Error en Redis "+ err);
});

clientRedis.hmset("Santiago","lat","-33.428","lng","-70.630");
clientRedis.hmset("Auckland","lat","-36.906","lng","174.672");
clientRedis.hmset("Sydney","lat","-33.869","lng","151.209");
clientRedis.hmset("Zurich","lat","47.390","lng","8.661");
clientRedis.hmset("Londres","lat","51.510","lng","-0.099");
clientRedis.hmset("Georgia","lat","32.539","lng","-83.381");



console.log('Ciudades cargadas.');
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


var server = http.createServer(app);
var io = socketIO(server);
server.listen(3000);
var cities = ['Auckland','Georgia','Londres','Sydney','Santiago','Zurich'];

io.on('connection',(socket)=>{
  console.log('Socket IO conectado');
  socket.on('update', async (msg)=>{
    console.log(msg);
    var dataPromise = cities.map((city)=> {
      return clientRedis.hmgetAsync(city, 'lat', 'lng').then((res) => {
        var key = '8079dd532fd7f37551b32a90b1ebfa38';
        var url = 'https://api.darksky.net/forecast/';
        return axios.get(url + key + '/' + res[0] + ',' + res[1]);
      });
    });

       var cityData = await Promise.all(dataPromise).then((resp)=>{
           return   resp.map((apiResp,index)=>{
                return {info:apiResp.data,name:cities[index]};
              });
       }).catch((err)=>{
         console.log(err);
       });
      console.log(cityData);
      socket.emit('cityData',cityData);




  });
});



